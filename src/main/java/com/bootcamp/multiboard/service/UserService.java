package com.bootcamp.multiboard.service;

import com.bootcamp.multiboard.aggregate.User;
import com.bootcamp.multiboard.dto.RegisterDto;

import java.util.List;

public interface UserService {

    String register(RegisterDto registerDto);
    List<User> findAll();
    User findUserByEmail(String email);
    void remove(String email);
    boolean exists(String email);
}
