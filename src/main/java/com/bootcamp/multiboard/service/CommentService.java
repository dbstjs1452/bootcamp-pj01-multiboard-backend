package com.bootcamp.multiboard.service;

import java.util.List;

import com.bootcamp.multiboard.aggregate.Comment;
import com.bootcamp.multiboard.service.sdo.CommentCdo;
import com.bootcamp.multiboard.shared.NameValueList;

public interface CommentService {
    
    String registerComment(String boardId, CommentCdo commentCdo);
    List<Comment> findCommentByBoardId(String boardId);
    void modifyComment(String commentId, NameValueList nameValues);
    void removeComment(String commentId);
    boolean existsComment(String commentId);
}
