package com.bootcamp.multiboard.service;

import com.bootcamp.multiboard.aggregate.Qna;
import com.bootcamp.multiboard.service.sdo.QnaCdo;
import com.bootcamp.multiboard.shared.NameValueList;

public interface QnaService {
    
    String registerQna(QnaCdo qnaCdo);
    Qna findQnaById(String qnaId);
    void modifyQna(String qnaId, NameValueList nameValues);
    void removeQna(String qnaId);
    boolean existsQna(String qnaId);
}
