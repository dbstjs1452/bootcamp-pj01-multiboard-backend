package com.bootcamp.multiboard.service;

import java.util.List;

import com.bootcamp.multiboard.aggregate.Board;
import com.bootcamp.multiboard.service.sdo.BoardCdo;
import com.bootcamp.multiboard.shared.NameValueList;

public interface BoardService {
    
    String registerBoard(BoardCdo boardCdo);
    List<Board> findAll();
    Board findBoardById(String boardId);
    void modifyBoard(String boardId, NameValueList nameValues);
    void removeBoard(String boardId);
    boolean existsBoard(String boardId);
}
