package com.bootcamp.multiboard.service;

import com.bootcamp.multiboard.aggregate.Notice;
import com.bootcamp.multiboard.service.sdo.NoticeCdo;
import com.bootcamp.multiboard.shared.NameValueList;

public interface NoticeService {
    
    String registerNotice(NoticeCdo noticeCdo);
    Notice findNoticeById(String noticeId);
    void modifyNotice(String noticeId, NameValueList nameValues);
    void removeNotice(String noticeId);
    boolean existsNotice(String noticeId);
}
