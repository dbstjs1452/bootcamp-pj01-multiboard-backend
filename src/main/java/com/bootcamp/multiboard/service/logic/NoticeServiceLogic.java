package com.bootcamp.multiboard.service.logic;

import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

import com.bootcamp.multiboard.aggregate.Notice;
import com.bootcamp.multiboard.service.NoticeService;
import com.bootcamp.multiboard.service.sdo.NoticeCdo;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.store.NoticeStore;

@Service
public class NoticeServiceLogic implements NoticeService{
    //
    private NoticeStore noticeStore;

    private NoticeServiceLogic(NoticeStore noticeStore){
        this.noticeStore = noticeStore;
    }

    @Override
    public String registerNotice(NoticeCdo noticeCdo){
        Notice notice = new Notice(noticeCdo);
        if(noticeStore.exists(notice.getId())){
            throw new IllegalArgumentException("notice already exists. " + notice.getId());
        }
        noticeStore.create(notice);
        return notice.getId();
    }

    @Override
    public Notice findNoticeById(String noticeId){
        Notice notice = noticeStore.retrieve(noticeId);
        if(notice == null){
            throw new NoSuchElementException("Notice id: "+ noticeId);
        }
        return notice;
    }

    @Override
    public void modifyNotice(String noticeId, NameValueList nameValueList){
        Notice notice = noticeStore.retrieve(noticeId);
        if(notice == null){
            return;
        }
        notice.modifyValues(nameValueList);
        noticeStore.update(notice);
    }

    @Override
    public void removeNotice(String noticeId){
        Notice notice = noticeStore.retrieve(noticeId);
        if(notice == null){
            return;
        }
        noticeStore.delete(notice);
    }

    @Override
    public boolean existsNotice(String noticeId){
        return noticeStore.exists(noticeId);
    }
}
