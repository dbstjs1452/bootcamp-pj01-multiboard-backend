package com.bootcamp.multiboard.service.logic;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

import com.bootcamp.multiboard.aggregate.Board;
import com.bootcamp.multiboard.service.BoardService;
import com.bootcamp.multiboard.service.sdo.BoardCdo;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.store.BoardStore;

@Service
public class BoardServiceLogic implements BoardService{
    //
    private BoardStore boardStore;

    private BoardServiceLogic(BoardStore boardStore){
        this.boardStore = boardStore;
    }

    @Override
    public String registerBoard(BoardCdo boardCdo){
        Board board = new Board(boardCdo);
        if(boardStore.exists(board.getId())){
            throw new IllegalArgumentException("board already exists. " + board.getId());
        }
        boardStore.create(board);
        return board.getId();
    }

    @Override
    public List<Board> findAll(){
        return boardStore.retrieveAll();
    }

    @Override
    public Board findBoardById(String boardId){
        Board board = boardStore.retrieve(boardId);
        if(board == null){
            throw new NoSuchElementException("Board id: " + boardId);
        }
        return board;
    }

    @Override
    public void modifyBoard(String boardId, NameValueList nameValueList){
        Board board = boardStore.retrieve(boardId);
        if(board == null){
            return;
            //throw new NoSuchBoardException()
        }
        board.modifyValues(nameValueList);
        boardStore.update(board);
    }
    
    @Override
    public void removeBoard(String boardId){
        Board board = boardStore.retrieve(boardId);
        if(board == null){
            return;
            //throw new NoSuchBoardException()
        }
        boardStore.delete(board);
    }

    @Override
    public boolean existsBoard(String boardId){
        return boardStore.exists(boardId);
    }
}
