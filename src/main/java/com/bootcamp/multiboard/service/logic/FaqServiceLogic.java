package com.bootcamp.multiboard.service.logic;

import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

import com.bootcamp.multiboard.aggregate.Faq;
import com.bootcamp.multiboard.service.FaqService;
import com.bootcamp.multiboard.service.sdo.FaqCdo;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.store.FaqStore;

@Service
public class FaqServiceLogic implements FaqService{
    //
    private FaqStore faqStore;

    private FaqServiceLogic(FaqStore faqStore){
        this.faqStore = faqStore;
    }

    @Override
    public String registerFaq(FaqCdo faqCdo){
        Faq faq = new Faq(faqCdo);
        if(faqStore.exists(faq.getId())){
            throw new IllegalArgumentException("faq already exists. " + faq.getId());
        }
        faqStore.create(faq);
        return faq.getId();
    }

    @Override
    public Faq findFaqById(String faqId){
        Faq faq = faqStore.retrieve(faqId);
        if(faq == null){
            throw new NoSuchElementException("Faq id: " + faqId);
        }
        return faq;
    }

    @Override
    public void modifyFaq(String faqId, NameValueList nameValues){
        Faq faq = faqStore.retrieve(faqId);
        if(faq == null){
            return;
        }
        faq.modifyValues(nameValues);
        faqStore.update(faq);
    }

    @Override
    public void removeFaq(String faqId){
        Faq faq = faqStore.retrieve(faqId);
        if(faq == null){
            return;
        }
        faqStore.delete(faq);
    }

    @Override
    public boolean existsFaq(String faqId){
        return faqStore.exists(faqId);
    }
}
