package com.bootcamp.multiboard.service.logic;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bootcamp.multiboard.aggregate.Board;
import com.bootcamp.multiboard.aggregate.Comment;
import com.bootcamp.multiboard.service.CommentService;
import com.bootcamp.multiboard.service.sdo.CommentCdo;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.store.BoardStore;
import com.bootcamp.multiboard.store.CommentStore;

@Service
public class CommentServiceLogic implements CommentService{
    //
    private CommentStore commentStore;
    private BoardStore boardStore;

    private CommentServiceLogic(CommentStore commentStore, BoardStore boardStore){
        this.commentStore = commentStore;
        this.boardStore = boardStore;
    }

    @Override
    public String registerComment(String boardId, CommentCdo commentCdo){
        Comment comment = new Comment(commentCdo);
        if(!boardStore.exists(comment.getBoardId())){
            //throw new NoSuchBoardException()
        }
        System.out.println("@check1");
        comment.setBoardId(boardId);
        System.out.println("@check2: "+boardId);
        commentStore.create(comment);
        System.out.println("@check3: "+comment);
        return comment.getId();
    }

    @Override
    public List<Comment> findCommentByBoardId(String boardId){
        //
        return null;
    }

    @Override
    public void modifyComment(String commentId, NameValueList nameValues){
        //
    }

    @Override
    public void removeComment(String commentId){
        Comment comment = commentStore.retrieve(commentId);
        if(comment == null){
            return;
            //throw new NoSuchCommentException()
        }
        commentStore.delete(comment);
    }

    @Override
    public boolean existsComment(String commentId){
        return commentStore.exists(commentId);
    }
}
