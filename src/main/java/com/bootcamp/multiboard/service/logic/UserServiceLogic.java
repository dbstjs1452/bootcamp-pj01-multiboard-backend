package com.bootcamp.multiboard.service.logic;

import com.bootcamp.multiboard.aggregate.User;
import com.bootcamp.multiboard.dto.RegisterDto;
import com.bootcamp.multiboard.service.UserService;
import com.bootcamp.multiboard.store.UserStore;
import com.bootcamp.multiboard.store.jpastore.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceLogic implements UserService {

    private final UserStore userStore;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public String register(RegisterDto registerDto){
        User user = new User();
        user.setPassword(bCryptPasswordEncoder.encode(registerDto.getPassword()));
        user.setEmail(registerDto.getEmail());
        user.setName(registerDto.getName());
        user.setPhoneNumber(registerDto.getPhoneNumber());
        user.setRoles("USER");
        if(userStore.exists(user.getEmail())){
            throw new IllegalArgumentException("Email already exists: " + user.getEmail());
        }
        return userStore.create(user);
    }
    @Override
    public List<User> findAll(){
        return userStore.retrieveAll();
    }
//    @Override
//    public User findUserById(long id){
//        return userRepository.findById(id).orElseThrow(()->{
//            return new IllegalArgumentException("User Id를 찾을 수 없습니다.");
//        });
//    }
    @Override
    public User findUserByEmail(String email){
        return userStore.retrieve(email);
    }

    @Override
    public void remove(String email){
        userStore.delete(email);
    }

    @Override
    public boolean exists(String email) {
        return userStore.exists(email);
    }
}
