package com.bootcamp.multiboard.service.logic;

import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

import com.bootcamp.multiboard.aggregate.Qna;
import com.bootcamp.multiboard.service.QnaService;
import com.bootcamp.multiboard.service.sdo.QnaCdo;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.store.QnaStore;

@Service
public class QnaServiceLogic implements QnaService{
    //
    private QnaStore qnaStore;

    private QnaServiceLogic(QnaStore qnaStore){
        this.qnaStore = qnaStore;
    }

    @Override
    public String registerQna(QnaCdo qnaCdo){
        Qna qna = new Qna(qnaCdo);
        if(qnaStore.exists(qna.getId())){
            throw new IllegalArgumentException("qna already exists. " + qna.getId());
        }
        qnaStore.create(qna);
        return qna.getId();
    }

    @Override
    public Qna findQnaById(String qnaId){
        Qna qna = qnaStore.retrieve(qnaId);
        if(qna == null){
            throw new NoSuchElementException("Qna id: " + qnaId);
        }
        return qna;
    }

    @Override
    public void modifyQna(String qnaId, NameValueList nameValues){
        Qna qna = qnaStore.retrieve(qnaId);
        if(qna == null){
            return;
        }
        qna.modifyValues(nameValues);
        qnaStore.update(qna);
    }

    @Override
    public void removeQna(String qnaId){
        Qna qna = qnaStore.retrieve(qnaId);
        if(qna == null){
            return;
        }
        qnaStore.delete(qna);
    }

    @Override
    public boolean existsQna(String qnaId){
        return qnaStore.exists(qnaId);
    }
}
