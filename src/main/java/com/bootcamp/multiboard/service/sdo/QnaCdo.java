package com.bootcamp.multiboard.service.sdo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QnaCdo implements Serializable{
    //
    private String title;
    private String content;
    private String writer;

    public static QnaCdo sample(){
        return new QnaCdo(
            "How to use this service",
            "Please explain how to use this service.",
            "nara@nextree.io"
        );
    }
}
