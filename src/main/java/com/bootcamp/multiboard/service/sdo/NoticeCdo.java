package com.bootcamp.multiboard.service.sdo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NoticeCdo implements Serializable{
    //
    private String title;
    private String content;
    private String writer;

    public static NoticeCdo sample(){
        return new NoticeCdo(
            "2023-01-13 Notice",
            "Notice Contents.",
            "admin@nextree.io"
        );
    }
}
