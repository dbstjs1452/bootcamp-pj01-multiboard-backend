package com.bootcamp.multiboard.service.sdo;

import com.bootcamp.multiboard.aggregate.Board;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentCdo {
    //
    //private String boardId;
    private String writer;
    private String content;
    private Board board;

//    public static CommentCdo sample(){
//        return new CommentCdo(
//            //"0001",
//            "minsoo@nextree.io",
//            "Hello, I am Minsoo"
//        );
//    }
}
