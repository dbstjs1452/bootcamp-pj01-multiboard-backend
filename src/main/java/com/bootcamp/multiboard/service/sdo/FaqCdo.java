package com.bootcamp.multiboard.service.sdo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FaqCdo implements Serializable{
    //
    private String question;
    private String answer;

    public static FaqCdo sample(){
        //
        return new FaqCdo(
            "Create and Edit",
            "You can create or edit your postings."
        );
    }
}
