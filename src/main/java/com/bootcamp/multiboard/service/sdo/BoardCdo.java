package com.bootcamp.multiboard.service.sdo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BoardCdo implements Serializable{
    //
    private String title;
    private String content;
    private String writer;

    public static BoardCdo sample(){
        return new BoardCdo(
            "First Posting",
            "This is my first posting.",
            "nara@nextree.io"
        );
    }
}
