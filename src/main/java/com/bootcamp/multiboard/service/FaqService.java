package com.bootcamp.multiboard.service;

import com.bootcamp.multiboard.aggregate.Faq;
import com.bootcamp.multiboard.service.sdo.FaqCdo;
import com.bootcamp.multiboard.shared.NameValueList;

public interface FaqService {
    
    String registerFaq(FaqCdo faqCdo);
    Faq findFaqById(String faqId);
    void modifyFaq(String faqId, NameValueList nameValues);
    void removeFaq(String faqId);
    boolean existsFaq(String faqId);
}
