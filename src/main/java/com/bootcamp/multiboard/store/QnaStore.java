package com.bootcamp.multiboard.store;

import com.bootcamp.multiboard.aggregate.Qna;

public interface QnaStore {
    //
    void create(Qna qna);
    Qna retrieve(String id);
    void update(Qna qna);
    void delete(Qna qna);
    boolean exists(String id);
}
