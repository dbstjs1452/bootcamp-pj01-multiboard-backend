package com.bootcamp.multiboard.store;

import com.bootcamp.multiboard.aggregate.Comment;

public interface CommentStore {
    //
    void create(Comment comment);
    Comment retrieve(String id);
    void update(Comment comment);
    void delete(Comment comment);
    boolean exists(String id);
}
