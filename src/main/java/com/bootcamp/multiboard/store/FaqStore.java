package com.bootcamp.multiboard.store;

import com.bootcamp.multiboard.aggregate.Faq;

public interface FaqStore {
    //
    void create(Faq faq);
    Faq retrieve(String id);
    void update(Faq faq);
    void delete(Faq faq);
    boolean exists(String id);
}
