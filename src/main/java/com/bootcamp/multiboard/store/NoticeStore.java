package com.bootcamp.multiboard.store;

import com.bootcamp.multiboard.aggregate.Notice;

public interface NoticeStore{
    //
    void create(Notice notice);
    Notice retrieve(String id);
    void update(Notice notice);
    void delete(Notice notice);
    boolean exists(String id);
}