package com.bootcamp.multiboard.store.jpastore.jpo;

import com.bootcamp.multiboard.aggregate.vo.BoardType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="POST")
public class PostJpo {
    
    @Id @Column(name="POST_ID")
    private String id;

    private String boardId;

    @Enumerated(EnumType.STRING)
    private BoardType boardType;
}
