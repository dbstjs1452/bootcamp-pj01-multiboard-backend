package com.bootcamp.multiboard.store.jpastore.jpo;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.aggregate.Board;
import com.bootcamp.multiboard.aggregate.Comment;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="COMMENT")
public class CommentJpo extends BaseTimeEntity{
    //
    @Id @Column(name="comment_id")
    private String id;

    @Column(nullable = false)
    private String writer;
    
    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;

    @ManyToOne
    @JoinColumn(name = "board_id")
    private BoardJpo board;
    
    public CommentJpo(Comment comment){
        BeanUtils.copyProperties(comment, this);
    }

    public Comment toDomain(){
        Comment comment = new Comment(getId());
        BeanUtils.copyProperties(this, comment);

        return comment;
    }

}
