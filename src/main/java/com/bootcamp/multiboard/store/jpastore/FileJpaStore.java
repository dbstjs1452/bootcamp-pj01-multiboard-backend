package com.bootcamp.multiboard.store.jpastore;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.bootcamp.multiboard.aggregate.File;
import com.bootcamp.multiboard.store.FileStore;
import com.bootcamp.multiboard.store.jpastore.jpo.FileJpo;
import com.bootcamp.multiboard.store.jpastore.repository.FileRepository;

@Repository
public class FileJpaStore implements FileStore{
    //
    private FileRepository fileRepository;

    public FileJpaStore(FileRepository fileRepository){
        this.fileRepository = fileRepository;
    }

    @Override
    public void create(File file){
        fileRepository.save(new FileJpo(file));
    }

    @Override
    public File retrieve(String id){
        Optional<FileJpo> fileJpo = fileRepository.findById(id);
        return fileJpo.map(FileJpo::toDomain).orElse(null);
    }

    @Override
    public void update(File file){
        fileRepository.save(new FileJpo(file));
    }

    @Override
    public void delete(File file){
        fileRepository.deleteById(file.getId());
    }

    @Override
    public boolean exists(String id){
        return fileRepository.existsById(id);
    }
}
