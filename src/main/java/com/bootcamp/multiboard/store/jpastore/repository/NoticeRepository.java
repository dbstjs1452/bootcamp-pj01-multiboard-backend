package com.bootcamp.multiboard.store.jpastore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.multiboard.store.jpastore.jpo.NoticeJpo;

public interface NoticeRepository extends JpaRepository<NoticeJpo, String>{
    
}
