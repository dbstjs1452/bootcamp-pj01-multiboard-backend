package com.bootcamp.multiboard.store.jpastore.repository;

import com.bootcamp.multiboard.aggregate.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    public User findByEmail(String email);
    public void deleteByEmail(String email);
    public boolean existsByEmail(String email);
}
