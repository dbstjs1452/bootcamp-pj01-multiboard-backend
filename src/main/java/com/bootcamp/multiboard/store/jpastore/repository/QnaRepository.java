package com.bootcamp.multiboard.store.jpastore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.multiboard.store.jpastore.jpo.QnaJpo;

public interface QnaRepository extends JpaRepository<QnaJpo, String> {
    
}
