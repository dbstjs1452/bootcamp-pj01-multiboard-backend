package com.bootcamp.multiboard.store.jpastore;

import com.bootcamp.multiboard.aggregate.User;
import com.bootcamp.multiboard.store.UserStore;
import com.bootcamp.multiboard.store.jpastore.repository.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserJpaStore implements UserStore {
    private UserRepository userRepository;

    public UserJpaStore(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    @Override
    public String create(User user) {
        userRepository.save(user);
        return "회원가입완료";
    }

    @Override
    public User retrieve(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> retrieveAll() {
        return userRepository.findAll();
    }

    @Override
    public void delete(String email) {
        userRepository.deleteByEmail(email);
    }

    @Override
    public boolean exists(String email) {
        return userRepository.existsByEmail(email);
    }
}
