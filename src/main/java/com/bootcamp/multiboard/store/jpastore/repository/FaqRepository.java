package com.bootcamp.multiboard.store.jpastore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.multiboard.store.jpastore.jpo.FaqJpo;

public interface FaqRepository extends JpaRepository<FaqJpo, String>{
    
}
