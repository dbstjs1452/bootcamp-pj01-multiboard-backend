package com.bootcamp.multiboard.store.jpastore;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.bootcamp.multiboard.aggregate.Comment;
import com.bootcamp.multiboard.store.CommentStore;
import com.bootcamp.multiboard.store.jpastore.jpo.CommentJpo;
import com.bootcamp.multiboard.store.jpastore.repository.CommentRepository;

@Repository
public class CommentJpaStore implements CommentStore{
    //
    private CommentRepository commentRepository;

    public CommentJpaStore(CommentRepository commentRepository){
        this.commentRepository = commentRepository;
    }

    @Override
    public void create(Comment comment){
        System.out.println("@@comment4: "+ comment.getBoardId());
        // CommentJpo jpo = new CommentJpo(comment);
        // System.out.println("@@comment5: "+ jpo.getBoardId());
        commentRepository.save(new CommentJpo(comment));
    }

    @Override
    public Comment retrieve(String id){
        Optional<CommentJpo> commentJpo = commentRepository.findById(id);
        return commentJpo.map(CommentJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Comment comment){
        commentRepository.save(new CommentJpo(comment));
    }

    @Override
    public void delete(Comment comment){
        commentRepository.deleteById(comment.getId());
    }

    @Override
    public boolean exists(String id){
        return commentRepository.existsById(id);
    }
}
