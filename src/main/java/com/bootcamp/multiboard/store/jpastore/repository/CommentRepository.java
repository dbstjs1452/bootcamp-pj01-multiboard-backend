package com.bootcamp.multiboard.store.jpastore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.multiboard.store.jpastore.jpo.CommentJpo;

public interface CommentRepository extends JpaRepository<CommentJpo, String>{
    
}
