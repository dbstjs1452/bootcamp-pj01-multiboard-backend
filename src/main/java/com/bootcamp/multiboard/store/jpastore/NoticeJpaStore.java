package com.bootcamp.multiboard.store.jpastore;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.bootcamp.multiboard.aggregate.Notice;
import com.bootcamp.multiboard.store.NoticeStore;
import com.bootcamp.multiboard.store.jpastore.jpo.NoticeJpo;
import com.bootcamp.multiboard.store.jpastore.repository.NoticeRepository;

@Repository
public class NoticeJpaStore implements NoticeStore{
    //
    private NoticeRepository noticeRepository;

    public NoticeJpaStore(NoticeRepository noticeRepository){
        this.noticeRepository = noticeRepository;
    }

    @Override
    public void create(Notice notice){
        noticeRepository.save(new NoticeJpo(notice));
    }

    @Override
    public Notice retrieve(String id){
        Optional<NoticeJpo> noticeJpo = noticeRepository.findById(id);
        return noticeJpo.map(NoticeJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Notice notice){
        noticeRepository.save(new NoticeJpo(notice));
    }

    @Override
    public void delete(Notice notice){
        noticeRepository.deleteById(notice.getId());
    }

    @Override
    public boolean exists(String id){
        return noticeRepository.existsById(id);
    }
}
