package com.bootcamp.multiboard.store.jpastore;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.bootcamp.multiboard.aggregate.Faq;
import com.bootcamp.multiboard.store.FaqStore;
import com.bootcamp.multiboard.store.jpastore.jpo.FaqJpo;
import com.bootcamp.multiboard.store.jpastore.repository.FaqRepository;

@Repository
public class FaqJpaStore implements FaqStore{
    //
    private FaqRepository faqRepository;

    public FaqJpaStore(FaqRepository faqRepository){
        this.faqRepository = faqRepository;
    }

    @Override
    public void create(Faq faq){
        faqRepository.save(new FaqJpo(faq));
    }

    @Override
    public Faq retrieve(String id){
        Optional<FaqJpo> faqJpo = faqRepository.findById(id);
        return faqJpo.map(FaqJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Faq faq){
        faqRepository.save(new FaqJpo(faq));
    }

    @Override
    public void delete(Faq faq){
        faqRepository.deleteById(faq.getId());
    }

    @Override
    public boolean exists(String id){
        return faqRepository.existsById(id);
    }

}
