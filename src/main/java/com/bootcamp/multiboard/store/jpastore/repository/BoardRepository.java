package com.bootcamp.multiboard.store.jpastore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.multiboard.store.jpastore.jpo.BoardJpo;

public interface BoardRepository extends JpaRepository<BoardJpo, String>{
    
}
