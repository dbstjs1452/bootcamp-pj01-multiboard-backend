package com.bootcamp.multiboard.store.jpastore.jpo;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.aggregate.Notice;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="NOTICE")
public class NoticeJpo extends BaseTimeEntity{
    //
    @Id @Column(name="NOTICE_ID")
    private String id;

    @Column(length = 500, nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;
    
    @Column(nullable = false)
    private String writer;
    
    @Column(columnDefinition = "integer default 0")
    private int hits;

    public NoticeJpo(Notice notice){
        BeanUtils.copyProperties(notice, this);
    }

    public Notice toDomain(){
        Notice notice = new Notice(getId());
        BeanUtils.copyProperties(this, notice);
        return notice;
    }
}
