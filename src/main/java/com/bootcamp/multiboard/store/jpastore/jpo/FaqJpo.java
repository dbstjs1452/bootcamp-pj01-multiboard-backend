package com.bootcamp.multiboard.store.jpastore.jpo;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.aggregate.Faq;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="FAQ")
public class FaqJpo extends BaseTimeEntity{
    //
    @Id @Column(name="FAQ_ID")
    private String id;

    @Column(nullable = false)
    private String question;
    
    @Column(columnDefinition = "TEXT", nullable = false)
    private String answer;

    public FaqJpo(Faq faq){
        BeanUtils.copyProperties(faq, this);
    }

    public Faq toDomain(){
        Faq faq = new Faq(getId());
        BeanUtils.copyProperties(this, faq);
        return faq;
    }
}
