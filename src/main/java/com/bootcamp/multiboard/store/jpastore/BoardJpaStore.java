package com.bootcamp.multiboard.store.jpastore;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.bootcamp.multiboard.aggregate.Board;
import com.bootcamp.multiboard.store.BoardStore;
import com.bootcamp.multiboard.store.jpastore.jpo.BoardJpo;
import com.bootcamp.multiboard.store.jpastore.repository.BoardRepository;

import lombok.extern.log4j.Log4j2;

@Repository
public class BoardJpaStore implements BoardStore{
    //
    private BoardRepository boardRepository;

    public BoardJpaStore(BoardRepository boardRepository){
        this.boardRepository = boardRepository;
    }

    @Override
    public void create(Board board){
        boardRepository.save(new BoardJpo(board));
    }

    @Override
    public List<Board> retrieveAll(){
        List<BoardJpo> boardJpos = boardRepository.findAll();
        return boardJpos.stream().map(BoardJpo::toDomain).collect(Collectors.toList());
    }

    @Override
    public Board retrieve(String id){
        Optional<BoardJpo> boardJpo = boardRepository.findById(id);
        return boardJpo.map(BoardJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Board board){
        boardRepository.save(new BoardJpo(board));
    }

    @Override
    public void delete(Board board){
        boardRepository.deleteById(board.getId());
    }

    @Override
    public boolean exists(String id){
        return boardRepository.existsById(id);
    }
}
