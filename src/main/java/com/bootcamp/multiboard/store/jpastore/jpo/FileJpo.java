package com.bootcamp.multiboard.store.jpastore.jpo;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.aggregate.File;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="FILE")
public class FileJpo{
    //
    @Id @Column(name="FILE_ID")
    private String id;
    private String boardId;    
    private String realName;
    private String filePath;
    private String fileSize;

    public FileJpo(File file){
        BeanUtils.copyProperties(file, this);
    }

    public File toDomain(){
        File file = new File(boardId, realName, filePath, fileSize);
        
        return file;
    }
}
