package com.bootcamp.multiboard.store.jpastore.jpo;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.aggregate.Qna;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="QNA")
public class QnaJpo extends BaseTimeEntity{
    //
    @Id @Column(name="QNA_ID")
    private String id;

    @Column(length = 500, nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;
    
    @Column(nullable = false)
    private String writer;
    
    @Column(columnDefinition = "integer default 0")
    private int hits;

    private boolean answer;
    private String comments;

    public QnaJpo(Qna qna){
        BeanUtils.copyProperties(qna, this);
    }

    public Qna toDomain(){
        Qna qna = new Qna(getId());
        BeanUtils.copyProperties(this, qna);
        return qna;
    }
}
