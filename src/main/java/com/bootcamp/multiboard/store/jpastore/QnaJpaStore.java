package com.bootcamp.multiboard.store.jpastore;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.bootcamp.multiboard.aggregate.Qna;
import com.bootcamp.multiboard.store.QnaStore;
import com.bootcamp.multiboard.store.jpastore.jpo.QnaJpo;
import com.bootcamp.multiboard.store.jpastore.repository.QnaRepository;

@Repository
public class QnaJpaStore implements QnaStore{
    //
    private QnaRepository qnaRepository;

    public QnaJpaStore(QnaRepository qnaRepository){
        this.qnaRepository = qnaRepository;
    }
    
    @Override
    public void create(Qna qna){
        qnaRepository.save(new QnaJpo(qna));
    }

    @Override
    public Qna retrieve(String id){
        Optional<QnaJpo> qnaJpo = qnaRepository.findById(id);
        return qnaJpo.map(QnaJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Qna qna){
        qnaRepository.save(new QnaJpo(qna));
    }

    @Override
    public void delete(Qna qna){
        qnaRepository.deleteById(qna.getId());
    }

    @Override
    public boolean exists(String id){
        return qnaRepository.existsById(id);
    }
}
