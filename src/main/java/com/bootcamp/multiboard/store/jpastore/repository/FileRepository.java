package com.bootcamp.multiboard.store.jpastore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.multiboard.store.jpastore.jpo.FileJpo;

public interface FileRepository extends JpaRepository<FileJpo, String> {
    
}
