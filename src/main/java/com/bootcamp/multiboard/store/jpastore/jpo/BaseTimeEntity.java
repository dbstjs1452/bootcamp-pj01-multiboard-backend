package com.bootcamp.multiboard.store.jpastore.jpo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.Getter;

@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseTimeEntity {
    //
    @CreatedDate
    @Column(updatable = false)
    private String createDate; // LocalDateTime > String 타입으로 변경

    @LastModifiedDate
    private String updateDate; // LocalDateTime > String 타입으로 변경

    @PrePersist // Entity insert 이전 실행
    public void onPrePersist(){
        this.createDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.updateDate = this.createDate;
    }

    @PreUpdate // Entity update 이후 실행
    public void onPreUpdate(){
    this.updateDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
