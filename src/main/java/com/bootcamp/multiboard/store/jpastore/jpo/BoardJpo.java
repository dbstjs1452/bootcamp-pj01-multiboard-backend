package com.bootcamp.multiboard.store.jpastore.jpo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.aggregate.Board;
import com.bootcamp.multiboard.aggregate.Comment;
import com.bootcamp.multiboard.aggregate.Post;
import com.bootcamp.multiboard.service.sdo.BoardCdo;
import com.google.gson.Gson;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="BOARD")
public class BoardJpo extends BaseTimeEntity{
    //
    @Id @Column(name="board_id")
    private String id;
    
    @Column(length = 500, nullable = false)
    private String title;
    
    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;
    
    @Column(nullable = false)
    private String writer;
    
    @Column(columnDefinition = "integer default 0")
    private int hits;

    @OneToMany(mappedBy = "board", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)//EAGER에 의해 게시글이 불러질때 댓글도 불러짐, CascadeType.REMOVE 게시글이 지워질때 댓글도 삭제
    @OrderBy("createDate asc")
    private List<CommentJpo> comments = new ArrayList<>();

    public BoardJpo(Board board){
        BeanUtils.copyProperties(board, this);
    }

    public Board toDomain(){
        Board board = new Board(getId());
        BeanUtils.copyProperties(this, board);

        return board;
    }

    public void addComments(CommentJpo commentJpo){
        this.comments.add(commentJpo);
    }
}
