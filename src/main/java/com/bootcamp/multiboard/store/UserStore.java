package com.bootcamp.multiboard.store;

import com.bootcamp.multiboard.aggregate.User;

import java.util.List;

public interface UserStore {
    String create(User user);
    User retrieve(String email);
    List<User> retrieveAll();
    void delete(String email);
    boolean exists(String email);
}
