package com.bootcamp.multiboard.store;

import com.bootcamp.multiboard.aggregate.File;

public interface FileStore {
    //
    void create(File file);
    File retrieve(String id);
    void update(File file);
    void delete(File file);
    boolean exists(String id);
}
