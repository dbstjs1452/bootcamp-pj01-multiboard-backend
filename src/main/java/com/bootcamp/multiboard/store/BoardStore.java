package com.bootcamp.multiboard.store;

import java.util.List;

import com.bootcamp.multiboard.aggregate.Board;

public interface BoardStore {
    //
    void create(Board board);
    List<Board> retrieveAll();
    Board retrieve(String id);
    void update(Board board);
    void delete(Board board);
    boolean exists(String id);
}
