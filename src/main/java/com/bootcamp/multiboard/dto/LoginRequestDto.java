package com.bootcamp.multiboard.dto;

import lombok.Data;

@Data
public class LoginRequestDto {
    private String email;
    private String password;
}
