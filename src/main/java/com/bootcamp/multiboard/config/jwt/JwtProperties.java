package com.bootcamp.multiboard.config.jwt;

public interface JwtProperties {

    String SECRET = "yoons0410";
    int EXPIRATION_TIME = 60000 * 60 * 10; // 10시간
    String TOKEN_PREFIX = "Bearer ";
    String HEADER_STRING = "Authorization";
}
