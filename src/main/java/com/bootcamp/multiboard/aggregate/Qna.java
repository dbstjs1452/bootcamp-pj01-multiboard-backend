package com.bootcamp.multiboard.aggregate;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.service.sdo.QnaCdo;
import com.bootcamp.multiboard.shared.NameValue;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.util.helper.DateUtil;
import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Qna extends Entity{
    //
    private String title;
    private String content;
    private String writer;
    private int hits;
    private boolean answer;
    private String comments;
    //private String createDate;
    //private String updateDate;

    public Qna(String id){
        super(id);
    }

    public Qna(QnaCdo qnaCdo){
        super();
        BeanUtils.copyProperties(qnaCdo, this);
        // this.hits = 0;
        // this.answer = false;
        // this.comments = null;
    }

    public void modifyValues(NameValueList nameValues){
        for(NameValue nameValue : nameValues.getNameValues()){
            String value = nameValue.getValue();
            switch(nameValue.getName()){
                case "title":
                    this.title = value;
                    break;
                case "content":
                    this.content = value;
                    break;
                case "comments":
                    this.comments = value;
            }
        }
    }

    public static Qna sample(){
        return new Qna(QnaCdo.sample());
    }

    public static void main(String[] args){
        System.out.println(new Gson().toJson(sample()));
    }
}
