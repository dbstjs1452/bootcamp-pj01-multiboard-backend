package com.bootcamp.multiboard.aggregate;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.service.sdo.CommentCdo;
import com.bootcamp.multiboard.shared.NameValue;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.util.helper.DateUtil;
import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Comment extends Entity{
    //
    private String boardId;
    private String writer;
    private String content;
    private String createDate;
    private String updateDate;

    public Comment(String id){
        super(id);
    }

    public Comment(CommentCdo commentCdo){
        super();
        BeanUtils.copyProperties(commentCdo, this);
    }

    public void modifyValues(NameValueList nameValues){
        for(NameValue nameValue : nameValues.getNameValues()){
            String value = nameValue.getValue();
            switch(nameValue.getName()){
                case "content":
                    this.content = value;
                    break;
            }
        }
    }

//    public static Comment sample(){
//        return new Comment(CommentCdo.sample());
//    }
//
//    public static void main(String[] args){
//        System.out.println(new Gson().toJson(sample()));
//    }
}
