package com.bootcamp.multiboard.aggregate;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.service.sdo.FaqCdo;
import com.bootcamp.multiboard.shared.NameValue;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.util.helper.DateUtil;
import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Faq extends Entity{
    //
    private String question;
    private String answer;
    //private String createDate;
    //private String updateDate;

    public Faq(String id){
        super(id);
    }

    public Faq(FaqCdo faqCdo){
        super();
        BeanUtils.copyProperties(faqCdo, this);
    }

    public void modifyValues(NameValueList nameValues){
        for(NameValue nameValue : nameValues.getNameValues()){
            String value = nameValue.getValue();
            switch(nameValue.getName()){
                case "question":
                    this.question = value;
                    break;
                case "answer":
                    this.answer = value;
                    break;
            }
        }
    }

    public static Faq sample(){
        return new Faq(FaqCdo.sample());
    }

    public static void main(String[] args){
        System.out.println(new Gson().toJson(sample()));
    }
}
