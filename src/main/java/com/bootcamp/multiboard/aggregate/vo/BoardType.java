package com.bootcamp.multiboard.aggregate.vo;

public enum BoardType {
    //
    BULLENTINBOARD,
    NOTICEBOARD,
    QNABOARD,
    FAQBOARD
}
