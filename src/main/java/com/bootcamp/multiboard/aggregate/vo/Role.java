package com.bootcamp.multiboard.aggregate.vo;

public enum Role {
    //
    ADMIN,
    MEMBER
}
