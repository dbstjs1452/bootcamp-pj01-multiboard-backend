package com.bootcamp.multiboard.aggregate.vo;

import com.google.gson.Gson;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Address {
    //
    private String zipCode;
    private String zipAddress;
    private String streetAddress;
    private String country;

    public String toString(){
        //
        StringBuilder builder = new StringBuilder();

        builder.append("zipcode:").append(zipCode);
        builder.append(", zip address:").append(zipAddress);
        builder.append(", street address:").append(streetAddress);
        builder.append(", country:").append(country);

        return builder.toString();
    }

    public static Address fromJson(String json){
        //
        return new Gson().fromJson(json, Address.class);
    }

    public static Address sample(){
        //
        return new Address(
            "08503",
            "서울시 금천구 가산디지털 1로 171",
            "가산SK V1 1801호",
            "South Korea"
        );
    }

    public static void main(String[] args){
        //
        System.out.println(new Gson().toJson(sample()));
    }
}
