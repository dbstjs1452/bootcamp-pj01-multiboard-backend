package com.bootcamp.multiboard.aggregate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class File extends Entity{
    //
    private String boardId;
    private String realName;
    private String filePath;
    private String fileSize;

    public File(String id){
        //
        super(id);
    }

    public File(String boardId, String realName, String filePath, String fileSize){
        super();
        this.boardId = boardId;
        this.realName = realName;
        this.filePath = filePath;
        this.fileSize = fileSize;
    }
}
