package com.bootcamp.multiboard.aggregate;

import com.bootcamp.multiboard.aggregate.vo.BoardType;

public class Post extends Entity{
    //
    private String boardId;
    private BoardType boardType;

    public Post(String id){
        //
        super(id);
    }

    public Post(String boardId, BoardType boardType){
        //
        super();
        this.boardId = boardId;
        this.boardType = boardType;
    }
}
