package com.bootcamp.multiboard.aggregate;

import org.springframework.beans.BeanUtils;

import com.bootcamp.multiboard.service.sdo.BoardCdo;
import com.bootcamp.multiboard.shared.NameValue;
import com.bootcamp.multiboard.shared.NameValueList;
import com.bootcamp.multiboard.util.helper.DateUtil;
import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Board extends Entity{
    //
    private String title;
    private String content;
    private String writer;
    private int hits;
    private String createDate;
    private String updateDate;

    public Board(String id){
        super(id);
    }

    public Board(BoardCdo boardCdo){
        super();
        BeanUtils.copyProperties(boardCdo, this);
    }

    public void modifyValues(NameValueList nameValues){
        for(NameValue nameValue : nameValues.getNameValues()){
            String value = nameValue.getValue();
            switch(nameValue.getName()){
                case "title":
                    this.title = value;
                    break;
                case "content":
                    this.content = value;
                    break;
            }
        }
    }

    public static Board sample(){
        return new Board(BoardCdo.sample());
    }

    public static void main(String[] args){
        System.out.println(new Gson().toJson(sample()));
    }
}
