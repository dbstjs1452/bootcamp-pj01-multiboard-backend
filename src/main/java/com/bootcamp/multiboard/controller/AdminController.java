package com.bootcamp.multiboard.controller;

import com.bootcamp.multiboard.aggregate.User;
import com.bootcamp.multiboard.config.auth.PrincipalDetails;
import com.bootcamp.multiboard.service.UserService;
import com.bootcamp.multiboard.store.jpastore.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AdminController {

    private final UserService userService;

    @GetMapping("/admin/users-info")
    public List<User> findUsersInfo(){
        return userService.findAll();
    }
}
