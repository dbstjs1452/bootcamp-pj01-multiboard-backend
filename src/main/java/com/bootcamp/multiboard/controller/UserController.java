package com.bootcamp.multiboard.controller;

import com.bootcamp.multiboard.aggregate.User;
import com.bootcamp.multiboard.config.auth.PrincipalDetails;
import com.bootcamp.multiboard.dto.RegisterDto;
import com.bootcamp.multiboard.service.UserService;
import com.bootcamp.multiboard.store.jpastore.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostMapping("/join")
    public String register(@RequestBody RegisterDto registerDto){
        return userService.register(registerDto);
    }

    @GetMapping("/user/info")
    public User findMyInfo(Authentication authentication){
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        return userService.findUserByEmail(principal.getUser().getEmail());
    }

//    @PutMapping("/user")
//    public void update(@RequestBody User user, Authentication authentication){
//        userService.update(user, principalDetail);
//        return user.getId();
//    }

    @DeleteMapping("/user/{email}")
    public void remove(@PathVariable(value = "email") String email){
        userService.remove(email);
    }
}
