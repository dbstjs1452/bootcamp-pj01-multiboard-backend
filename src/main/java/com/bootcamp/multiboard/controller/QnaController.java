package com.bootcamp.multiboard.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.multiboard.aggregate.Qna;
import com.bootcamp.multiboard.service.QnaService;
import com.bootcamp.multiboard.service.sdo.QnaCdo;
import com.bootcamp.multiboard.shared.NameValueList;

@RestController
@RequestMapping("/qna")
public class QnaController {
    
    private QnaService qnaService;

    public QnaController(QnaService qnaService){
        this.qnaService = qnaService;
    }

    @PostMapping
    public String register(@RequestBody QnaCdo qnaCdo){
        return qnaService.registerQna(qnaCdo);
    }

    @GetMapping("/{id}")
    public Qna findById(@PathVariable(value = "id") String qnaId){
        return qnaService.findQnaById(qnaId);
    }

    @PutMapping("/{id}")
    public void modify(@PathVariable(value = "id") String qnaId, NameValueList nameValueList){
        qnaService.modifyQna(qnaId, nameValueList);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") String qnaId){
        qnaService.removeQna(qnaId);
    }
}
