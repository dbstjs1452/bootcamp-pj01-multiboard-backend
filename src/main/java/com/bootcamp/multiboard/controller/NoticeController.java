package com.bootcamp.multiboard.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.multiboard.aggregate.Notice;
import com.bootcamp.multiboard.service.NoticeService;
import com.bootcamp.multiboard.service.sdo.NoticeCdo;
import com.bootcamp.multiboard.shared.NameValueList;

@RestController
@RequestMapping("/notice")
public class NoticeController {
    
    private NoticeService noticeService;

    public NoticeController(NoticeService noticeService){
        this.noticeService = noticeService;
    }

    @PostMapping
    public String register(@RequestBody NoticeCdo noticeCdo){
        return noticeService.registerNotice(noticeCdo);
    }

    @GetMapping("/{id}")
    public Notice findById(@PathVariable(value = "id") String noticeId){
        return noticeService.findNoticeById(noticeId);
    }

    @PutMapping("/{id}")
    public void modify(@PathVariable(value = "id") String noticeId, NameValueList nameValueList){
        noticeService.modifyNotice(noticeId, nameValueList);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") String noticeId){
        noticeService.removeNotice(noticeId);
    }
}
