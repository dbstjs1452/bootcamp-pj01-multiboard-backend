package com.bootcamp.multiboard.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.multiboard.service.CommentService;
import com.bootcamp.multiboard.service.sdo.CommentCdo;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class CommentController {
    
    private CommentService commentService;

    public CommentController(CommentService commentService){
        this.commentService = commentService;
    }
    
    @PostMapping("/board/{boardId}/comment")
    public String register(@PathVariable(value = "boardId") String boardId, @RequestBody CommentCdo commentCdo){
        return commentService.registerComment(boardId, commentCdo);
    }
    
    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") String commentId){
        commentService.removeComment(commentId);
    }

}
