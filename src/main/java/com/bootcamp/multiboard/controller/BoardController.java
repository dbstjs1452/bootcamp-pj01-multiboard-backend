package com.bootcamp.multiboard.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.multiboard.aggregate.Board;
import com.bootcamp.multiboard.service.BoardService;
import com.bootcamp.multiboard.service.sdo.BoardCdo;
import com.bootcamp.multiboard.shared.NameValueList;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/board")
public class BoardController {
    
    private BoardService boardService;

    public BoardController(BoardService boardService){
        this.boardService = boardService;
    }

    @PostMapping
    public String register(@RequestBody BoardCdo boardCdo){
        return boardService.registerBoard(boardCdo);
    }

    @GetMapping("/all")
    public List<Board> findAll(){
        return boardService.findAll();
    }

    @GetMapping("/{id}")
    public Board findById(@PathVariable(value = "id") String boardId){
        return boardService.findBoardById(boardId);
    }

    @PutMapping("/{id}")
    public void modify(@PathVariable(value = "id") String boardId, @RequestBody NameValueList nameValueList){
        boardService.modifyBoard(boardId, nameValueList);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") String boardId){
        boardService.removeBoard(boardId);
    }
}