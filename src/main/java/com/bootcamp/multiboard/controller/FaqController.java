package com.bootcamp.multiboard.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.multiboard.aggregate.Faq;
import com.bootcamp.multiboard.service.FaqService;
import com.bootcamp.multiboard.service.sdo.FaqCdo;
import com.bootcamp.multiboard.shared.NameValueList;

@RestController
@RequestMapping("/faq")
public class FaqController {
    //
    private FaqService faqService;

    public FaqController(FaqService faqService){
        this.faqService = faqService;
    }

    @PostMapping
    public String register(@RequestBody FaqCdo faqCdo){
        return faqService.registerFaq(faqCdo);
    }

    @GetMapping("/{id}")
    public Faq findById(@PathVariable(value = "id") String faqId){
        return faqService.findFaqById(faqId);
    }

    @PutMapping("/{id}")
    public void modify(@PathVariable(value = "id") String faqId, NameValueList nameValueList){
        faqService.modifyFaq(faqId, nameValueList);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") String faqId){
        faqService.removeFaq(faqId);
    }
}
